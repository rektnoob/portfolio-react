import RootLayout from "./layout";
import Home from "./home/page";

export default function Main() {
  return (
    <RootLayout>
      <Home></Home>
    </RootLayout>
  );
}
