"use client";

import Link from "next/link";
import { RefObject, useEffect, useRef, useState } from "react";

import { throttle } from "lodash";

import Welcome from "../components/welcome";

import "./home.scss";
import Footer from "../components/footer/footer";
import SkillzMatrix from "../components/skillz-matrix/skillz-matrix";

export default function Home() {
  const snapContainerRef: RefObject<HTMLElement> = useRef(null);
  const mainBgAnchorRef: RefObject<HTMLElement> = useRef(null);
  const codeEditorAnchorRef: RefObject<HTMLDivElement> = useRef(null);
  const designAnchorRef: RefObject<HTMLDivElement> = useRef(null);
  const bgAnchors: RefObject<HTMLElement>[] = [codeEditorAnchorRef, designAnchorRef];

  const [codeState, setCodeState] = useState({ left: -10000, top: -10000, bgPosX: 0, bgPosY: 0 });
  const [designState, setDesignState] = useState({ left: -10000, top: -10000, bgPosX: 0, bgPosY: 0 });

  const bgState = [codeState, designState];
  const bgStateSetters = [setCodeState, setDesignState];
  const throttleDuration = 20;

  useEffect(() => {
    document.addEventListener("mousemove", updatePortholeMousePos);
    document.addEventListener("mouseover", updatePortholeMousePos);
    document.addEventListener("touchmove", updatePortholeTouchPos);
    return () => {
      document.removeEventListener("mousemove", updatePortholeMousePos);
      document.removeEventListener("mouseover", updatePortholeMousePos);
      document.removeEventListener("touchmove", updatePortholeTouchPos);
    };
  }, []);

  const throttledUpdate = throttle((pos) => {
    updatePortholePos(pos.x, pos.y);
  }, throttleDuration);

  function updatePortholeMousePos(event: MouseEvent) {
    event.stopPropagation();
    throttledUpdate({ x: event.pageX, y: event.pageY });
  }

  function updatePortholeTouchPos(event: TouchEvent) {
    const pageX = event.touches.item(0)?.pageX ?? -1000;
    const pageY = event.touches.item(0)?.pageY ?? -1000;
    event.stopPropagation();
    throttledUpdate({ x: pageX, y: pageY });
  }

  function updatePortholePos(pageX: number, pageY: number) {
    const scrollY = snapContainerRef.current?.scrollTop ?? 0;
    const adjustX = mainBgAnchorRef.current?.offsetLeft ?? 0;
    const adjustY = mainBgAnchorRef.current?.offsetTop ?? 0;

    bgAnchors.forEach((bgAnchor, idx) => {
      const bgPorthole = bgAnchor.current?.lastElementChild;
      const portholeHalfHeight = (bgPorthole?.clientHeight ?? 2) / 2;
      const portholeHalfWidth = (bgPorthole?.clientWidth ?? 2) / 2;
      const offsetLeft = bgAnchor.current?.offsetLeft ?? 0;
      const offsetTop = bgAnchor.current?.offsetTop ?? 0;
      const anchorTop = offsetTop + adjustY - portholeHalfHeight;
      const anchorBottom = adjustY + offsetTop + (bgAnchor.current?.clientHeight ?? 0);

      if (
        pageY + scrollY > anchorTop &&
        pageX > offsetLeft + adjustX - portholeHalfWidth &&
        pageY + scrollY - portholeHalfHeight < anchorBottom
      ) {
        const left = pageX - portholeHalfWidth - adjustX - offsetLeft;
        const top = pageY - portholeHalfHeight - adjustY + scrollY - offsetTop;
        const bgPosX = adjustX + offsetLeft;
        const bgPosY = adjustY - scrollY + offsetTop;

        bgStateSetters[idx]({ left: left, top: top, bgPosX: bgPosX, bgPosY: bgPosY });
      } else {
        bgStateSetters[idx]({ left: -10000, top: -10000, bgPosX: bgState[idx].bgPosX, bgPosY: bgState[idx].bgPosY });
      }
    });
  }

  return (
    <main ref={snapContainerRef} className="home snap-container">
      <section className="intro fade">
        <h2 className="welcome">
          <Welcome />
          <SkillzMatrix />
        </h2>
        <aside ref={mainBgAnchorRef} className="background-anchor">
          <div ref={codeEditorAnchorRef} className="code-editor-ss-anchor">
            <div className="code-editor-screenshot"></div>
            <div className="code-editor-mask"></div>
            <div
              className="code-editor-porthole"
              style={{
                left: codeState.left,
                top: codeState.top,
                backgroundPositionX: codeState.bgPosX,
                backgroundPositionY: codeState.bgPosY,
              }}
            ></div>
          </div>

          <div ref={designAnchorRef} className="design-ss-anchor">
            <div className="design-screenshot"></div>
            <div className="design-ss-mask"></div>
            <div
              className="design-screenshot-porthole"
              style={{
                left: designState.left,
                top: designState.top,
                backgroundPositionX: designState.bgPosX,
                backgroundPositionY: designState.bgPosY,
              }}
            ></div>
          </div>
        </aside>
      </section>

      <section className="experience">
        <aside className="content">
          <h1>Experience</h1>
          <article className="summary">
            Over fifteen years of experience building successful web applications across various industries including
            Retirement Investing, Cloud Services, and Geospatial Intelligence via Satellite Imagery.
          </article>
          <Link className="button" href="/projects">
            View Projects
          </Link>
        </aside>
      </section>

      <section className="about">
        <aside className="content">
          <h1>About</h1>
          <article className="summary">
            <p>
              Angular expert and Full Stack, Senior Software Engineer with strong experience applying a mixture of
              languages and technologies to create applications and websites that solve specific client needs.
            </p>
            <p>
              Lead developer and team player, delivering numerous successful projects by designing, architecting,
              developing, mentoring, reviewing code, testing, and technical writing.
            </p>
            <p>Favorite tasks include UI/UX design, UI Architecture, and front-end development.</p>
          </article>
          <a
            className="button"
            href="/docs/Chris Demaria - Senior Full Stack Engineer.pdf"
            target="_blank"
            rel="noopener noreferrer"
          >
            My R&eacute;sum&eacute;
          </a>
        </aside>
      </section>

      <div className="footer">
        <Footer tagLine="" showFade={true} />
      </div>
    </main>
  );
}
