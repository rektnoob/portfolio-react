import Carousel from "../components/carousel/carousel";
import GmsDetail from "../components/gms-detail/gms-detail";
import ProjectSummary from "../components/project-summary/project-summary";
import { ProjectSummaryDef } from "../components/project-summary/project-summary-def";
import VideoPlayer from "../components/video-player/video-player";
import "./projects.scss";

const globalMonitoring: ProjectSummaryDef = {
  title: "Global Monitoring Dashboard",
  subTitle: "Ursa Space Systems",
};

const ursaPlatform: ProjectSummaryDef = {
  title: "Ursa Platform UI",
  subTitle: "Ursa Space Systems",
};

const gisFeatureLib: ProjectSummaryDef = {
  title: "GIS Feature Library Architecture",
  subTitle: "Ursa Space Systems",
};

const buildUp: ProjectSummaryDef = {
  title: "Buildings Up Challenge Proposal (Video)",
  subTitle: "Village of Montour Falls",
};

const naviProximity: ProjectSummaryDef = {
  title: "Proximity",
  subTitle: "NaviSite",
};

interface VideoUrls {
  [name: string]: string;
}

export default function Projects() {
  const videoUrls: VideoUrls = {
    ursaGms: "https://player.vimeo.com/video/787712970?h=906553a6aa#t=1s&amp;quality=1080p",
    ursaPlatform: "https://player.vimeo.com/video/589643051?h=5d8eb2e89d#t=5s&amp;quality=1080p",
    buildingsUp: "https://www.youtube.com/embed/KyDr6nJBm3s",
    naviProximity: "https://www.youtube.com/embed/FowlJq6DRhw?start=86",
  };

  return (
    <main className="projects">
      <h1>Projects</h1>
      <section className="snap-container">
        <section className="project">
          <aside className="project-summary">
            <ProjectSummary>
              <ProjectSummary.Header>
                <h2>{globalMonitoring.title}</h2>
                <h3>{globalMonitoring.subTitle}</h3>
              </ProjectSummary.Header>
              <ProjectSummary.Details>
                <article>
                  A subscription service allowing for near real time monitoring of vessels, objects, and change around
                  the globe.
                </article>
                <article>
                  An exciting project involving fast paced prototyping, cross-team coordination, UI/UX design, user
                  testing, and development of a UI to visualize AI analysis of satellite radar imagery.
                </article>
              </ProjectSummary.Details>
            </ProjectSummary>
          </aside>

          <aside className="carousel-wrapper">
            <div className="carousel">
              <Carousel>
                <img
                  className="screenshot"
                  alt="Ursa Global Monitoring Screenshot"
                  src="/images/Ursa - Project01 - SS - GMS.webp"
                />
                <img
                  className="screenshot"
                  loading="lazy"
                  alt="Ursa Global Monitoring East Asia"
                  src="/images/gms_east_asia_satellite_basemap.webp"
                />
                <GmsDetail />
                <VideoPlayer src={videoUrls.ursaGms} />
              </Carousel>
            </div>
            <p>Swipe for more details.</p>
          </aside>
        </section>

        <section className="project">
          <aside className="project-summary">
            <ProjectSummary>
              <ProjectSummary.Header>
                <h2>{ursaPlatform.title}</h2>
                <h3>{ursaPlatform.subTitle}</h3>
              </ProjectSummary.Header>
              <ProjectSummary.Details>
                <article>
                  A fun series of redesigns and feature additions to Ursa&apos;s internal and external facing web
                  portal.
                </article>
              </ProjectSummary.Details>
              <ProjectSummary.MoreDetails>
                <article>
                  <p>
                    Early on, we redesigned the UI for searching the Imagery Catalog. I co-authored the Hi-Fi mockups in
                    Figma. Then, I implemented those changes using Angular, CSS3, SASS, HTML5, and GraphQL.
                  </p>
                  <p>
                    The original Imagery Catalog search design had two separate slide-outs where filters could be
                    applied. On the left side of the screen there was a slide-out for basic filters. On the right, was
                    another slide-out for advanced filters. The action button, that executed the search, was only
                    available in the advanced filters slide-out. So, kicking off an imagery search wasn&apos;t intuitive
                    especially if you just wanted basic filters.
                  </p>
                  <p>
                    We consolidated all of the basic and advanced filter parameters into a single slide-out and made the
                    search button always visible and styled it with the primary color. We avoided clutter by reducing
                    all of the non-essential, advanced filters to a <em>More</em> button. This allowed them to be
                    dynamically added to the form by clicking <em>More</em>.
                  </p>
                  <p>
                    I improved the code by refactoring the complex search logic out of the advanced filters component
                    and into an Angular service to facilitate reuse.
                  </p>
                </article>
                <article>
                  Later we did a more comprehensive redesign involving a new design system with a fresh color scheme and
                  better typography. I co-created the design system in Figma. Then, I implemented design system using
                  Angular Material&apos;s Theming System and SASS.
                </article>
                <article>
                  <p>
                    Another fun project I worked on was implementing an integration with <em>Stripe.com</em> to allow
                    the purchasing of Synthetic Aperture Radar (SAR) imagery from Ursa&apos;s constellation of data
                    partner satellites.
                  </p>
                  <p>
                    The New Image Tasking Module, which allowed users to task a satellite to collect imagery for a given
                    location and future time frame, was also integrated with <em>Stripe.com</em> to faclitate these
                    purchases.
                  </p>
                </article>
              </ProjectSummary.MoreDetails>
            </ProjectSummary>
          </aside>
          <aside className="carousel-wrapper">
            <div className="carousel">
              <Carousel>
                <img
                  className="screenshot"
                  alt="Imagery Search Query Screenshot"
                  src="/images/Imagery Search Query.webp"
                />
                <img
                  className="screenshot"
                  alt="Imagery Search Results Screenshot"
                  loading="lazy"
                  src="/images/Imagery Search Results.webp"
                />
                <VideoPlayer src={videoUrls.ursaPlatform} />
              </Carousel>
            </div>
            <p>Swipe for more details.</p>
          </aside>
        </section>

        <section className="project">
          <aside className="project-summary">
            <ProjectSummary>
              <ProjectSummary.Header>
                <h2>{gisFeatureLib.title}</h2>
                <h3>{gisFeatureLib.subTitle}</h3>
              </ProjectSummary.Header>
              <ProjectSummary.Details>
                <article>Full Stack Architecture Design for GIS Feature Library.</article>
                <article>
                  System design including: authentication, authorization, data model, data schema, data storage, API
                  (GraphQL, and REST), and frontend using Kubernetes, NestJS, GraphQL, Mongoose, Jest, NodeJS, MongoDB,
                  PVCs, NGINx, Angular, and Apollo.
                </article>
              </ProjectSummary.Details>
            </ProjectSummary>
          </aside>
          <aside className="single-page">
            <img
              className="screenshot"
              alt="GIS Feature Library Architecture"
              src="/images/site_lib_architecture_diagram.webp"
            />
          </aside>
        </section>

        <section className="project">
          <aside className="project-summary">
            <ProjectSummary>
              <ProjectSummary.Header>
                <h2>{buildUp.title}</h2>
                <h3>{buildUp.subTitle}</h3>
              </ProjectSummary.Header>
              <ProjectSummary.Details>
                <article>
                  Videography and Video Editing for a short film that accompanied a grant application for funds to
                  support households in completing projects that improve and prepare homes for efficiency improvements
                  and electrification.
                </article>
              </ProjectSummary.Details>
            </ProjectSummary>
          </aside>
          <aside className="single-page">
            <VideoPlayer src={videoUrls.buildingsUp} title="Buildings Up Challenge Proposal (Video)" />
          </aside>
        </section>

        <section className="project">
          <aside className="project-summary">
            <ProjectSummary>
              <ProjectSummary.Header>
                <h2>{naviProximity.title}</h2>
                <h3>{naviProximity.subTitle}</h3>
              </ProjectSummary.Header>
              <ProjectSummary.Details>
                <article>
                  Co-authored and deployed Naviste Proximity client portal, using Angular, SASS, CSS3, HTML5, Python,
                  Docker, and Kubernetes.
                </article>
                <article>
                  Navisite&apos;s Proximity™ client portal provides clear, consistent, and reliable visibility into the
                  managed services and monitoring tools that are installed in the client&apos;s environment, including
                  Incident Events, Change Request, Infrastructure Visibility, Backup, Replication, Antivirus, and
                  Intrusion Detection Services.
                </article>
              </ProjectSummary.Details>
            </ProjectSummary>
          </aside>

          <aside className="carousel-wrapper">
            <div className="carousel">
              <Carousel>
                <img
                  className="screenshot"
                  alt="Proximity Dashboard Screenshot"
                  src="/images/proximity_dashboard.webp"
                />
                <img
                  className="screenshot"
                  alt="Proximity Sevices Backup Screenshot"
                  loading="lazy"
                  src="/images/prox_services_backups.webp"
                />
                <img
                  className="screenshot"
                  alt="Proximity Infrastructure Configuration"
                  loading="lazy"
                  src="/images/proximity_infra_conf.webp"
                />
                <img
                  className="screenshot"
                  alt="Proximity Billable Bandwidth Graph"
                  loading="lazy"
                  src="/images/prox_billable_band_graph.webp"
                />
                <VideoPlayer src={videoUrls.naviProximity} title="Navisite Proximity" />
              </Carousel>
            </div>
            <p>Swipe for more details.</p>
          </aside>
        </section>
      </section>
    </main>
  );
}
