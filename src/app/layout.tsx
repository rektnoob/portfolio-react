import type { Metadata } from "next";
import { StrictMode } from "react";

import NavBar from "./components/navbar";
import "../../src/styles.scss";
import { Analytics } from "@vercel/analytics/react";

export const metadata: Metadata = {
  title: "Chris Demaria | Portfolio",
  description: "Showcasing Recent Projects, Skills, Resume, and contact information.",
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body>
        <StrictMode>
          <NavBar />
          {children}
          <Analytics />
        </StrictMode>
      </body>
    </html>
  );
}
