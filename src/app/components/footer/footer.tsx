import { useEffect, useState } from "react";
import { Contact } from "../contact-info/contact";
import ContactInfo from "../contact-info/contact-info";

import "./footer.scss";

interface ContactMap {
  [name: string]: Contact;
}

const email = "christopher.david.demaria@gmail.com";
const emailer = `mailto:${email}`;
const linkedIn = "https://www.linkedin.com/in/christopher-demaria-b4634110/";
const gitlab = "https://gitlab.com/rektnoob";
const twitter = "https://twitter.com/pungentpollywog";

const contactMap: ContactMap = {
  email: {
    label: "Email",
    href: emailer,
    showCopyBtn: true,
    copyText: email,
    showActionBtn: true,
    actionBtnLabel: "Send",
    openNewTab: false,
  },
  linkedIn: {
    label: "Linked In",
    href: linkedIn,
    showCopyBtn: true,
    copyText: linkedIn,
    showActionBtn: false,
    openNewTab: true,
  },
  gitlab: {
    label: "Gitlab",
    href: gitlab,
    showCopyBtn: true,
    copyText: gitlab,
    showActionBtn: false,
    openNewTab: true,
  },
  twitter: {
    label: "Twitter",
    href: twitter,
    showCopyBtn: true,
    copyText: twitter,
    showActionBtn: false,
    openNewTab: true,
  },
};

interface FooterProps {
  tagLine: string;
  showFade: boolean;
}

export default function Footer(props: FooterProps) {
  const [tagLine, setTagLine] = useState("");
  const [showFade, setShowFade] = useState(true);

  useEffect(() => {
    setTagLine(props.tagLine);
    setShowFade(props.showFade);
  }, [props]);

  return (
    <section className={showFade ? "footer fade" : "footer"}>
      {tagLine && (
        <section className="tag-line">
          <h5>{tagLine}</h5>
        </section>
      )}
      <section className="contact-info-list">
        <ContactInfo contact={contactMap.email}>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            {/* Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. */}
            <path d="M48 64C21.5 64 0 85.5 0 112c0 15.1 7.1 29.3 19.2 38.4L236.8 313.6c11.4 8.5 27 8.5 38.4 0L492.8 150.4c12.1-9.1 19.2-23.3 19.2-38.4c0-26.5-21.5-48-48-48H48zM0 176V384c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V176L294.4 339.2c-22.8 17.1-54 17.1-76.8 0L0 176z" />
          </svg>
        </ContactInfo>
        <ContactInfo contact={contactMap.linkedIn}>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
            {/* Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. */}
            <path d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z" />
          </svg>
        </ContactInfo>
        <ContactInfo contact={contactMap.gitlab}>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            {/* Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. */}
            <path d="M503.5 204.6L502.8 202.8L433.1 21.02C431.7 17.45 429.2 14.43 425.9 12.38C423.5 10.83 420.8 9.865 417.9 9.57C415 9.275 412.2 9.653 409.5 10.68C406.8 11.7 404.4 13.34 402.4 15.46C400.5 17.58 399.1 20.13 398.3 22.9L351.3 166.9H160.8L113.7 22.9C112.9 20.13 111.5 17.59 109.6 15.47C107.6 13.35 105.2 11.72 102.5 10.7C99.86 9.675 96.98 9.295 94.12 9.587C91.26 9.878 88.51 10.83 86.08 12.38C82.84 14.43 80.33 17.45 78.92 21.02L9.267 202.8L8.543 204.6C-1.484 230.8-2.72 259.6 5.023 286.6C12.77 313.5 29.07 337.3 51.47 354.2L51.74 354.4L52.33 354.8L158.3 434.3L210.9 474L242.9 498.2C246.6 500.1 251.2 502.5 255.9 502.5C260.6 502.5 265.2 500.1 268.9 498.2L300.9 474L353.5 434.3L460.2 354.4L460.5 354.1C482.9 337.2 499.2 313.5 506.1 286.6C514.7 259.6 513.5 230.8 503.5 204.6z" />
          </svg>
        </ContactInfo>
        <ContactInfo contact={contactMap.twitter}>
          {/* <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z" />
          </svg> */}

          <svg xmlns="http://www.w3.org/2000/svg" height="2em" viewBox="0 0 512 512">
            {/* Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc.  */}
            <path d="M389.2 48h70.6L305.6 224.2 487 464H345L233.7 318.6 106.5 464H35.8L200.7 275.5 26.8 48H172.4L272.9 180.9 389.2 48zM364.4 421.8h39.1L151.1 88h-42L364.4 421.8z" />
          </svg>
        </ContactInfo>
      </section>
      <section className="title">
        <h1>Chris Demaria</h1>
        <h5>Full Stack</h5>
        <h5>Senior Software Engineer</h5>
      </section>
    </section>
  );
}
