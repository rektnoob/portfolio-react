import "./gms-detail.scss";

export default function GmsDetail() {
  return (
    <section className="gms-detail">
      <section className="content">
        <header>
          <h3>Global Monitoring Dashboard</h3>
          <h4>for Ursa Space Systems</h4>
        </header>
        <section className="details">
          <aside className="summary">
            <h6>Figma was the design tool of choice.</h6>
            <h6>Development Stack included Angular 14, Mapbox JS, CSS3, Dart SASS, and HTML5.</h6>
          </aside>
          <aside className="icons1">
            <section className="icon-wrapper">
              <img src="/svg/Angular-logo.svg" />
            </section>
            <section className="icon-wrapper">
              <img src="/svg/figma-logo.svg" />
            </section>
            <section className="icon-wrapper">
              <img src="/svg/Mapbox-logo.svg" />
            </section>
          </aside>
          <aside className="icons2">
            <section className="icon-wrapper">
              <img src="/svg/css3-logo.svg" />
            </section>
            <section className="icon-wrapper">
              <img src="/svg/Sass-logo.svg" />
            </section>
            <section className="icon-wrapper">
              <img src="/svg/html5-logo.svg" />
            </section>
          </aside>
          <aside className="icon-desc">
            <p>Angular, Figma, Mapbox, CSS3, SASS, HTML5</p>
          </aside>
        </section>
      </section>
    </section>
  );
}
