import { RefObject, useEffect, useRef } from "react";

import "./welcome.scss";

import anime from "animejs/lib/anime.es.js";

export default function Welcome() {
  const selfRef: RefObject<HTMLElement> = useRef(null);

  function animateSelf() {
    const styles = `
          transform-origin: 50% 100%;  
          display: inline-block;
        `;

    if (selfRef.current && selfRef.current.textContent) {
      selfRef.current.innerHTML = selfRef.current.textContent.replace(
        /\S/g,
        `<span class='letter' style="${styles}">$&</span>`
      );
    }

    anime.timeline({ loop: false }).add({
      targets: "div.welcome div.greeting .letter",
      scale: [0, 1],
      duration: 1500,
      elasticity: 600,
      delay: (el: HTMLElement, i: number) => 45 * (i + 1),
    });
  }

  useEffect(() => {
    animateSelf();
  }, []);

  return (
    <div className="welcome">
      <div className="greeting">
        Greetings! I&#39;m{" "}
        <span className="highlight letters" ref={selfRef}>
          Chris Demaria
        </span>
        .
      </div>
      <div>I&#39;m a Full-Stack, Senior Software Engineer.</div>
    </div>
  );
}
