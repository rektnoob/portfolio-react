"use client";

import { useEffect, useState } from "react";

import "./video-player.scss";
import Spinner from "../spinner/spinner";

interface PlayerProps {
  src: string;
  title?: string;
}

export default function VideoPlayer(props: PlayerProps) {
  const [src, setSrc] = useState("");
  const [title, setTitle] = useState("");

  useEffect(() => {
    setSrc(props.src);
    setTitle(props.title ?? "video-player");
  }, [props]);

  return (
    <section className="video-player-wrapper">
      <figure className="video-player">
        <iframe title={title} loading="lazy" src={src}></iframe>
        <div className="spinner">
          <Spinner />
        </div>
      </figure>
    </section>
  );
}
