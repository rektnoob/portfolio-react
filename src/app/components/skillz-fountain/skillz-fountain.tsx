import { useEffect } from "react";

import anime from "animejs/lib/anime.es.js";

import "./skillz-fountain.scss";
import AngularLogo from "../logos/angular-logo";
import Css3Logo from "../logos/css3-logo";
import DockerLogo from "../logos/docker-logo";
import HtmlLogo from "../logos/html-logo";
import SassLogo from "../logos/sass-logo";
import ReactLogo from "../logos/react-logo";
import SvelteLogo from "../logos/svelte-logo";
import PythonLogo from "../logos/python-logo";

const range = 1000;

interface SkillzFountainProps {
  delay?: number;
  loop?: boolean;
}

export default function SkillzFountain(props: SkillzFountainProps = { delay: 0, loop: false }) {
  useEffect(() => {
    if (props.delay) {
      setTimeout(kickoff, props.delay);
    } else {
      kickoff();
    }
  }, []);

  function kickoff() {
    console.log("props.loop", props.loop);
    if (props.loop) {
      console.log("looping");
      setInterval(launch, 10000);
    } else {
      console.log("single launch");
      launch();
    }
  }

  function launch() {
    const randEnd = () => anime.random(-range, range);

    anime({
      targets: "div.skillz-fountain svg",
      delay: anime.stagger(750),
      translateY: [0, randEnd],
      translateX: [0, randEnd],
      easing: "easeInOutSine",
      duration: 3000,
      scale: [0, 30],
      opacity: [0.5, 0],
    });
  }

  return (
    <div className="skillz-fountain">
      <Css3Logo></Css3Logo>
      <HtmlLogo></HtmlLogo>
      <SassLogo></SassLogo>
      <AngularLogo></AngularLogo>
      <ReactLogo></ReactLogo>
      <SvelteLogo></SvelteLogo>
      <PythonLogo></PythonLogo>
      <DockerLogo></DockerLogo>
    </div>
  );
}
