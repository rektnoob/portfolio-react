import styles from "./css3-logo.module.scss";

export default function Css3Logo() {
  return (
    <svg
      className={styles.logo}
      width="42"
      height="48"
      viewBox="0 0 42 48"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_633_1988)">
        <path
          d="M3.55016 42.9717L0.0400391 0.0805664L41.96 0.16676L38.2484 42.9717L21.1583 47.9195L3.55016 42.9717Z"
          fill="#455569"
        />
        <path d="M21.1582 43.7773V4.16553L38.4211 4.22299L35.3427 39.6636L21.1582 43.7773Z" fill="#828D9B" />
        <path
          d="M34.1342 9.22852H7.57825L8.29765 14.4062H20.8993L8.49913 19.7282L9.2182 24.7335H27.4882L26.8263 31.7236L20.6404 32.932L15.0302 31.4938L14.5985 27.5238H9.44842L10.1387 35.7801L21.187 38.8581L31.7752 35.4925L33.1275 19.354H21.9348L34.1339 14.2913L34.1342 9.22852Z"
          fill="#C5CFEC"
        />
      </g>
      <defs>
        <clipPath id="clip0_633_1988">
          <rect width="41.92" height="48" fill="white" transform="translate(0.0400391)" />
        </clipPath>
      </defs>
    </svg>
  );
}
