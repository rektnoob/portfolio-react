import "./html-logo.scss";

export default function HtmlLogo() {
  return (
    <svg
      className="html-logo"
      width="42"
      height="48"
      viewBox="0 0 42 48"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_635_1996)">
        <path
          d="M3.64294 42.9719L0.151489 0.0800781L41.8485 0.166271L38.1566 42.9719L21.1573 47.9197L3.64294 42.9719Z"
          fill="#455569"
        />
        <path d="M21.1573 43.7774V4.16504L38.3284 4.2225L35.2664 39.6635L21.1573 43.7774Z" fill="#828D9B" />
        <path
          d="M33.6066 14.061L34.0928 8.8252H7.6781L9.13749 24.8774H27.3675L26.6526 31.7239L20.8143 33.3058L14.8903 31.5799L14.5756 27.495H9.33791L10.0535 35.8375L20.814 38.858L31.6602 35.8375L33.1196 19.5554H13.9743L13.4306 14.061H33.6066Z"
          fill="#C5CFEC"
        />
      </g>
      <defs>
        <clipPath id="clip0_635_1996">
          <rect width="41.697" height="48" fill="white" transform="translate(0.151489)" />
        </clipPath>
      </defs>
    </svg>
  );
}
