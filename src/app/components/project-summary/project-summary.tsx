import "./project-summary.scss";

function Header({ children }: { children: React.ReactNode }) {
  return <>{children}</>;
}

function Details({ children }: { children: React.ReactNode }) {
  return <section className="desc">{children}</section>;
}

function MoreDetails({ children }: { children: React.ReactNode }) {
  return (
    <section className="desc">
      <details className="more">
        <summary>More details...</summary>
        {children}
      </details>
    </section>
  );
}

export default function ProjectSummary({ children }: { children: React.ReactNode }) {
  return <aside className="project-summary">{children}</aside>;
}

ProjectSummary.Header = Header;
ProjectSummary.Details = Details;
ProjectSummary.MoreDetails = MoreDetails;
