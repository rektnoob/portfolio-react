export interface ProjectSummaryDef {
  title: string;
  subTitle: string;
}
