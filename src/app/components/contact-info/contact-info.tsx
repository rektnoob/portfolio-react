import { useEffect, useState } from "react";
import { Contact } from "./contact";
import "./contact-info.scss";

export default function ContactInfo({ contact, children }: { contact: Contact; children: React.ReactNode }) {
  const [href, setHref] = useState("");
  const [rel, setRel] = useState("");
  const [target, setTarget] = useState("");
  const [copyNotityDefaultText, setCopyNotifyDefaultText] = useState("Copy");
  const [copyNotify, setCopyNotify] = useState("Copy");

  function copy(ev: React.MouseEvent) {
    ev.stopPropagation();
    ev.preventDefault();
    navigator.clipboard.writeText(contact.copyText ?? "");
    setCopyNotify("Copied");
    setTimeout(() => setCopyNotify(copyNotityDefaultText), 1000);
  }

  useEffect(() => {
    setHref(contact.href);
    if (contact.openNewTab) {
      setRel("noopener noreferrer");
      setTarget("_blank");
    }
    setCopyNotifyDefaultText(`Copy ${contact.label}`);
    setCopyNotify(`Copy ${contact.label}`);
  }, [contact]);

  return (
    <section className="contact-info">
      <a href={href} target={target} rel={rel}>
        <div className="icon">{children}</div>
        <h5>{contact.label}</h5>
        {contact.showActionBtn && <div className="btn">{contact.actionBtnLabel}</div>}
        {contact.showCopyBtn && (
          <div className="btn copy" onClick={(ev) => copy(ev)}>
            Copy
            <span className="tooltip top">{copyNotify}</span>
          </div>
        )}
        <div className={copyNotify === "Copied" ? "success show" : "success"}>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            {/* Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. */}
            <path d="M256 48a208 208 0 1 1 0 416 208 208 0 1 1 0-416zm0 464A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM369 209c9.4-9.4 9.4-24.6 0-33.9s-24.6-9.4-33.9 0l-111 111-47-47c-9.4-9.4-24.6-9.4-33.9 0s-9.4 24.6 0 33.9l64 64c9.4 9.4 24.6 9.4 33.9 0L369 209z" />
          </svg>
        </div>
      </a>
    </section>
  );
}
