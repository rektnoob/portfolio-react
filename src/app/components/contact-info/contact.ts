export interface Contact {
  label: string;
  href: string;
  showCopyBtn: boolean;
  copyText?: string;
  showActionBtn: boolean;
  actionBtnLabel?: string;
  openNewTab: boolean;
}
