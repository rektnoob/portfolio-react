import { Children } from "react";

import "./carousel.scss";

export default function Carousel({ children }: { children: React.ReactNode }) {
  const arrayChildren = Children.toArray(children);

  return (
    <section className="slider" id="slider">
      {Children.map(arrayChildren, (child, idx) => (
        <section className="child" key={idx}>
          {child}
        </section>
      ))}
    </section>
  );
}
