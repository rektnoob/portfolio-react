import { useEffect } from "react";

import anime from "animejs/lib/anime.es.js";

import AngularLogo from "../logos/angular-logo";
import Css3Logo from "../logos/css3-logo";
import DockerLogo from "../logos/docker-logo";
import HtmlLogo from "../logos/html-logo";
import PythonLogo from "../logos/python-logo";
import ReactLogo from "../logos/react-logo";
import SassLogo from "../logos/sass-logo";
import SvelteLogo from "../logos/svelte-logo";

import "./skillz-matrix.scss";

export default function SkillzMatrix() {
  function animateSkillz() {
    anime
      .timeline({ loop: false })
      .add({
        targets: ".matrix .logo",
        delay: anime.stagger(500),
        easing: "easeInOutSine",
        opacity: [0, 1],
        duration: 200,
      })
      .add({
        targets: ".matrix .logo",
        delay: anime.stagger(750),
        easing: "easeInOutSine",
        duration: 5000,
        translateY: [0, 2500],
        opacity: [1, 0],
      });
  }

  useEffect(() => {
    setTimeout(animateSkillz, 1000);
  }, []);

  return (
    <div className="matrix">
      <span className="logo">
        <Css3Logo></Css3Logo>
      </span>
      <span className="logo">
        <SassLogo></SassLogo>
      </span>
      <span className="logo">
        <HtmlLogo></HtmlLogo>
      </span>
      <span className="logo">
        <ReactLogo></ReactLogo>
      </span>
      <span className="logo">
        <AngularLogo></AngularLogo>
      </span>
      <span className="logo">
        <SvelteLogo></SvelteLogo>
      </span>
      <span className="logo">
        <PythonLogo></PythonLogo>
      </span>
      <span className="logo">
        <DockerLogo></DockerLogo>
      </span>
    </div>
  );
}
