"use client";

import Footer from "../components/footer/footer";
import SkillzFountain from "../components/skillz-fountain/skillz-fountain";
import "./contact.scss";

export default function Contact() {
  return (
    <main className="contact">
      <Footer tagLine="Let's solve some problems together!" showFade={false} />
      <SkillzFountain />
    </main>
  );
}
